import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { Store } from "@ngrx/store";
import * as SocialShare from "nativescript-social-share";
import * as Toast from "nativescript-toasts";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { AppState } from "../app.module";
import { Noticia, NuevaNoticiaAction } from "../domain/noticias-state.model";
import { NoticiasService } from "../domain/noticias.service";
import { Color, View } from "tns-core-modules/ui/core/view";
import { toBase64String } from "@angular/compiler/src/output/source_map";

@Component({
    selector: "Search",
    templateUrl: "./search.component.html",
    
})
export class SearchComponent implements OnInit {
    resultados: Array<string>;
   // @ViewChild("layout") layout: ElementRef;

    constructor(
        private noticias: NoticiasService,
        private store: Store<AppState>
        ) {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        this.store.select((state) => state.noticias.sugerida)
        .subscribe((data) => {
            const f = data;
            if (f != null) {
                Toast.show({text: "Sugerimos leer: " + f.titulo, duration: Toast.DURATION.SHORT});
            }
        });
        // Init your component properties here.
        // this.noticias.agregar("Hola 1");
        // this.noticias.agregar("Hola 2");
        // this.noticias.agregar("Hola 3");
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }
    onItemTap(args): void {
        this.store.dispatch(new NuevaNoticiaAction(new Noticia(args.view.bindingContext)));
        // console.dir(x);
    }

    onLongPress(s): void {
        console.log(s);
        SocialShare.shareText(s, "Asunto: compartido desde el curso!");
    }

    buscarAhora(s: string) {
        console.dir("buscarAhora" + s);
        this.noticias.buscar(s).then((r: any) => {
            console.log("resultados buscarAhora: " + JSON.stringify(r));
            this.resultados = r;
        }, (e) => {
            console.log("error buscarAhora " + e);
            Toast.show({text: "Error en la busqueda", duration: Toast.DURATION.SHORT});
        });
    
        // const layout = <View>this.layout.nativeElement;
        // layout.animate({
        //     backgroundColor: new Color("blue"),
        //     duration: 300,
        //     delay: 150
        // }).then(() => layout.animate({
        //     backgroundColor: new Color("white"),
        //     duration: 300,
        //     delay: 150
        // }));
    }
}
