import { Component, EventEmitter, Output, OnInit, Input } from "@angular/core";
import { textAlignmentProperty } from "tns-core-modules/ui/text-base";

@Component({
    selector: "SearchForm",
    template: `
    <TextField [(NgModule)]="textFieldValue" hint="Quiero algo nuevo..."></TextField>
    <Button text="Buscar!" (tap)="onButtonTap()"></Button>
    `,
})

export class SearchFormComponent {
    textFieldValue: string = "";
    @Output() search: EventEmitter<string> = new EventEmitter();
    

    onButtonTap(): void {
        console.log(this.textFieldValue);
        if (this.textFieldValue.length > 2) {
            this.search.emit(this.textFieldValue);
        }
    }
}