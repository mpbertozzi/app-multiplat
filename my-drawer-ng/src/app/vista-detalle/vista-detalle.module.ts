import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { VistaDetalleRoutingModule } from "./vista-detalle-routing.module";
import { VistaDetalleComponent } from "./vista-detalle.component";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        VistaDetalleRoutingModule
    ],
    declarations: [
        VistaDetalleComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class VistaDetalleModule { }
