import { Component, OnInit } from "@angular/core";
import * as toast from "nativescript-toast";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import * as dialogs from "tns-core-modules/ui/dialogs";

@Component({
    selector: "Settings",
    templateUrl: "./settings.component.html"
})
export class SettingsComponent implements OnInit {

    constructor() {
        // Use the component constructor to inject providers.
    }

    doLater(fn) { setTimeout(fn, 1000); }
    
    ngOnInit(): void {
        // this.doLater(() =>
        //     dialogs.action("Mensaje", "Cancelar!", ["Opcion 1", "Opcion 2"])
        //         .then((result) => {
        //                             console.log("resultado: " + result);
        //                             if (result === "Opcion 1") {
        //                                 this.doLater(() =>
        //                                     dialogs.alert({
        //                                         title: "Titulo 1 ",
        //                                         message: "msj 1",
        //                                         okButtonText: "btn 1"
        //                                     }).then(() => console.log("Cerrado 1")));
        //                             } else if (result === "Opcion 2") {
        //                                 this.doLater(() =>
        //                                     dialogs.alert({
        //                                         title: "Titulo 2",
        //                                         message: "msj 2",
        //                                         okButtonText: "btn 2"
        //                                     }).then(() => console.log("Cerrado 2")));
        //                             }
        //         }));
        // const toastOptions: toast.ToastOptions = {text: "Hello World", duration: toast.ScaleDownPusherTransition.SHORT};
        // this.doLater(() => toast.show(toastOptions));
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }
}
